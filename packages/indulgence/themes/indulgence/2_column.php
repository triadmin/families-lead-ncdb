<?php     
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
	
	<div class="row">

		<div class="large-12 columns">
			<?php     
			$a = new Area('Main');
			$a->setBlockWrapperStart('<div class="">');
  			$a->setBlockWrapperEnd('</div>');
			$a->display($c);
			?>	

			<div class="row">
				<?php     
				$a = new Area('Column');
				$a->setBlockWrapperStart('<div class="large-6 columns">');
	  			$a->setBlockWrapperEnd('</div>');
				$a->display($c);
				?>	
			
			</div>

			<div class="clear"></div>

		</div>	
	
	</div>

	
<?php     $this->inc('elements/footer.php'); ?>
