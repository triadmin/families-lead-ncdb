<?php     
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>


	<div class="row">
		
		<div class="large-8 columns">		
			<?php     
			$a = new Area('Main');
			$a->setBlockWrapperStart('<div class="main-block">');
  			$a->setBlockWrapperEnd('</div>');
			$a->display($c);
			?>	
		

			<div class="clear"></div>

		</div>	
	


		<div class="right-sidebar-container large-4 columns ">

			<div class="row">
		
				<?php     
				$a = new Area('Sidebar');
			  	$a->setBlockWrapperStart('<div class="large-12 columns side-block">');
	  			$a->setBlockWrapperEnd('</div>');
				$a->display($c);
				?>
				
			</div>
		
		</div>

	</div>
	
<?php     $this->inc('elements/footer.php'); ?>
