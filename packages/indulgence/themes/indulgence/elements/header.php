<?php      defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php    echo LANGUAGE?>">

<head>

<link rel="stylesheet" media="screen" type="text/css" href="<?php  echo $this->getStyleSheet('foundation-4.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php  echo $this->getStyleSheet('view.css')?>" />
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php  echo $this->getStyleSheet('foundation-4-ie8.css')?>" />
<![endif]-->
<link rel="stylesheet" media="screen" type="text/css" href="<?php     echo $this->getStyleSheet('main.css')?>" />
<link rel="stylesheet" media="screen" type="text/css" href="<?php     echo $this->getStyleSheet('typography.css')?>" />
<link href='http://fonts.googleapis.com/css?family=Lora:400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<?php      Loader::element('header_required'); ?>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><![endif]-->

<!-- FAV AND TOUCH ICONS -->
<link rel="shortcut icon" href="<?php echo $this->getThemePath()?>/images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://tri.triportal.org/assets/images/icons/device-icon-144-new.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://tri.triportal.org/assets/images/icons/device-icon-72-new.png">
<link rel="apple-touch-icon-precomposed" href="http://tri.triportal.org/assets/images/icons/device-icon-57-new.png">

</head>

<body>

<div id="indulgence-container"> <!-- Container to target all CSS to make sure there are no clashes with exisiting C5 styles -->

	<div class="row">	
		<div class="main-container large-12 columns">

			<div class="header-container">	
				<div class="row">
					<div class="header large-12 columns">
						
						<a href="/">
							<div class="logo-container">
								<?php     
								$a = new GlobalArea('Site Logo');
								$a->setBlockLimit(1);
								$a->display();
								?>
							</div>
						</a>

						<div class="header-right">
							<?php     
							$a = new GlobalArea('Secondary Header Content');
							$a->setBlockLimit(3);
							$a->display();
							?>
						</div>	


					<div class="clear"></div>		

					<a id="MenuToggle" class="menu-button pull-right" href="javascript:void(0)">&nbsp;</a>
						<div class="navigation" id="menu">
							<?php     
							$a = new GlobalArea('Header Nav');
							$a->setBlockLimit(1);
							$a->display();
							?>
						</div>	

					</div>
					<div class="clear"></div>
				</div>
			</div>
	

