<?php      defined('C5_EXECUTE') or die("Access Denied."); ?>
			<div class="clear"></div>
		
			<div class="footer">
				<div class="footer-credits">
					<p class="footer-copyright">Footer content</p>
					<div class="clear"></div>
				</div>	
			</div>	

		</div>
		
	</div>


<!-- end indulgence-container-->
</div>
<div class="clear"></div>


<script type="text/javascript">
    $('#MenuToggle').click(function () {
        $('#menu').slideToggle('slow', function () {
        });

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
</script>


<script type="text/javascript">
	$(function () {
		$('.header ul li ul').hide();
		$('.header ul li').hover(function () {
			$('ul', this).stop(true, true).slideToggle(400);
		});
	});
</script>


<?php  Loader::element('footer_required'); ?>

</body>
</html>