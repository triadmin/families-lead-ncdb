<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class indulgencePackage extends Package {

 protected $pkgHandle = 'indulgence';
 protected $appVersionRequired = '5.5.1';
 protected $pkgVersion = '0.9.4';
 
 public function getPackageDescription() {
  return t("Installs the indulgence theme.");
 }
 
 public function getPackageName() {
  return t("indulgence");
 }
 
 public function install() {
  $pkg = parent::install();  
  PageTheme::add('indulgence', $pkg); 

   // auto-install page types
		
						
	$pt = CollectionType::getByHandle('blog_entry');
		if(!is_object($pt)) {
			$data['ctHandle'] = 'blog_entry';
			$data['ctName'] = t('Blog Entry');
			$hpt = CollectionType::add($data, $pkg);
		} 

	$pt = CollectionType::getByHandle('home');
		if(!is_object($pt)) {
			$data['ctHandle'] = 'home';
			$data['ctName'] = t('Home');
			$hpt = CollectionType::add($data, $pkg);
		} 

 	$pt = CollectionType::getByHandle('full');
		if(!is_object($pt)) {
			$data['ctHandle'] = 'full';
			$data['ctName'] = t('Full');
			$hpt = CollectionType::add($data, $pkg);
		} 

 	$pt = CollectionType::getByHandle('2_column');
		if(!is_object($pt)) {
			$data['ctHandle'] = '2_column';
			$data['ctName'] = t('Two Column');
			$hpt = CollectionType::add($data, $pkg);
		} 

	$pt = CollectionType::getByHandle('3_column');
		if(!is_object($pt)) {
			$data['ctHandle'] = '3_column';
			$data['ctName'] = t('Three Column');
			$hpt = CollectionType::add($data, $pkg);
		}

	$pt = CollectionType::getByHandle('left_sidebar');
		if(!is_object($pt)) {
			$data['ctHandle'] = 'left_sidebar';
			$data['ctName'] = t('Left Sidebar');
			$hpt = CollectionType::add($data, $pkg);
		} 


 }

}