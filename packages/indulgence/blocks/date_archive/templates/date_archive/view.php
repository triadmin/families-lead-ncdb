<?php   defined('C5_EXECUTE') or die("Access Denied.");  ?>
<div class="date-archive-container" id="main-content-sidebar-archives">
	<div class="block-surround"><h3><?php   echo $title ?></h3></div>
	<?php  
	if ($firstPost) { 
		$startDt = new DateTime();
		$firstPost = new DateTime($firstPost->format('m/01/Y'));
		$first = date_diff($startDt,$firstPost);
		$first = $first->m;
		
		if($first > $numMonths) {
			$first = $numMonths;
		}	
		
		$startDt->modify('-'.$first.' months');
		$workingDt = $startDt;
		$year = $workingDt->format('Y');
		?>
		<br />
		<h4><?php   echo $year;?></h4>
		<ul>
			<?php   
			$i=0;
			while(true) {
				if($workingDt->format('Y') > $year) {
					$year = $workingDt->format('Y');
					?></ul>
					<br />
					<h4><?php  echo $year?></h4><ul><?php  
				}
				?>
				<li>
				<?php   if($target instanceof Page) { ?>
						<a href="<?php   echo $navigation->getLinkToCollection($target)."?year=".$workingDt->format('Y'). "&month=".$workingDt->format('m') ?>" <?php   echo ($workingDt->format('m-Y') == $_REQUEST['month']?'class="selected"':'')?>><span class="arrow">&#8594;</span><?php   echo $workingDt->format('M') ?></a>
				<?php   } else { ?>
						<?php   echo $workingDt->format('M') ?>
				<?php   } ?>
				</li>
				<?php  
				if($i >= $first) { break; }
				$workingDt->modify('+1 month');
				$i++;
			} ?>
		</ul>
<?php   } ?>
</div>