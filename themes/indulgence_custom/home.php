<?php     
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
	
	<div class="slider-container main-block">
			
		<?php   
    	$a = new Area('Slider Area');
        $a->setBlockLimit(1);
        $a->display($c);
    	 ?>
			
	</div>

	<div class="row">
		
		<div class="large-12 columns">

			<div class="clear"></div>		

			<div class="row">	
				<?php     
				$a = new Area('Column');
			  	$a->setBlockWrapperStart('<div class="large-4 columns main-block">');
	  			$a->setBlockWrapperEnd('</div>');
				$a->display($c);
				?>
			</div>

			<div class="clear"></div>

			<div class="row">
			 	<?php     
				$a = new Area('Main');
			  	$a->setBlockWrapperStart('<div class="large-12 columns main-block">');
	  			$a->setBlockWrapperEnd('</div>');
				$a->display($c);
				?>
			</div>

			<div class="clear"></div>

			<div class="row">
				<ul class="large-block-grid-3">	
					<?php     
					$a = new Area('Blog List');
					$a->display($c);
					?>
				</ul>
			</div>

		</div>	


		<div class="clear"></div>

	</div>
	
	
<?php     $this->inc('elements/footer.php'); ?>
