var app = {};

app.init = function() 
{
    app.init_menu();
    app.init_tabs();
    app.hilight_search_results();
    app.init_breadcrumbs();
}

app.init_menu = function()
{
    $('.header ul li ul').hide();
    $('.header ul li').hover(function () {
        $('ul', this).stop(true, true).slideToggle(400);
    });
}

app.init_breadcrumbs = function()
{
  $('.swp-breadcrumbs a').first().hide();
  $('.swp-breadcrumbs span.delim').first().hide();
}

app.init_tabs = function() 
{
    //
    // Find all blocks with class tab_content and turn them into tabs.
    //
    $('.tab-content').hide();
    
    var tabs = '<div class="ccm-tags-display"><ul class="ccm-tag-list">';
    $('.tab-content').each( function( index )
    {
        var $this = $(this);
        var tab_title = $this.find('h2').html();
        if ( tab_title != null )
        {  
          tab_title = tab_title.replace(/<\/?[^>]+(>|$)/g, "");
        }  
        var tab_no = parseInt(index + 1);
        tabs += '<li class="tab tab' + tab_no + '"><a href="#" class="tab-link tab" data-tab="tab' + tab_no + '">' + tab_title + '</a></li>';
        
    });
    tabs += '</ul></div><div class="clear"></div>';
    $( tabs ).insertBefore( '#tab1' );
    
    // Turn on a tab - if we don't have a tab
    // URL parameter, turn on the first tab.
    if ( typeof query_string.tab != 'undefined' )
    {
      console.log(query_string.tab);
      $('.tab-content').eq(query_string.tab - 1).show();
      $('.ccm-tag-list li').eq(query_string.tab - 1).addClass('active'); 
    } else { 
      $('.tab-content').first().show();
      $('.ccm-tag-list li').first().addClass('active');
    }  
    
    // Wire up tab links
    $('.tab-link').on('click', function() {
      var $this = $(this);
      var tab_id = $this.data('tab');
      
      if ( $this.attr('href') !== '#' )
      {
        var tab_id_number = tab_id.charAt(3);
        window.location = $this.attr('href') + '?tab=' + tab_id_number;
        return false;
      } else {
        $('.tab-content').hide();
        $('#' + tab_id).fadeIn(1500);

        $('.tab-link').parent().removeClass('active');
        $this.parent().addClass('active');
        
        window.scrollTo(0,0);
        return false;
      }  
    });
}

app.hilight_search_results = function()
{
    var q = app.get_url_vars();
    var search_param = q.q;
    if ( typeof search_param != 'undefined' )
    {    
        search_param = search_param.replace( /%20/g, ' ' );
        $("body p").highlight( search_param );
    }    
}

app.get_url_vars = function()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var query_string = function()
{
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    	// If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    	// If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    	// If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
}();