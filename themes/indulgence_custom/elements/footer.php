<?php      defined('C5_EXECUTE') or die("Access Denied."); ?>
			<div class="clear"></div>
		
			<div class="footer">
				<div style="width: 33%; float: left; padding: 12px;">
					<p>
              <?php     
							$a = new GlobalArea('Footer Content 1');
							$a->setBlockLimit(3);
							$a->display();
							?>
          </p>
				</div>	
        <div style="width: 33%; float: left; padding: 12px;">
					<p>
              <?php     
							$a = new GlobalArea('Footer Content 2');
							$a->setBlockLimit(3);
							$a->display();
							?>
          </p>
				</div>	
        <div style="width: 33%; float: left; padding: 12px;">
          <p>
              <?php     
							$a = new GlobalArea('Footer Content 3');
							$a->setBlockLimit(3);
							$a->display();
							?>
          </p>
				</div>	
        <div class="clear"></div>
        <div style="padding: 12px;">
          <?php     
          $a = new GlobalArea('Footer Content 4');
          $a->setBlockLimit(3);
          $a->display();
          ?>
        </div>
			</div>	

		</div>
		
	</div>


<!-- end indulgence-container-->
</div>
<div class="clear"></div>


<script type="text/javascript">
    $('#MenuToggle').click(function () {
        $('#menu').slideToggle('slow', function () {
        });

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
</script>

<?php
// Load custom JavaScript if we're not in edit mode.
global $c;
if ( !$c->isEditMode() ) :
?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        app.init();
    });
</script>
<?php endif; ?>


<?php  Loader::element('footer_required'); ?>

<!-- Google Tracking Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1427386-11', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>