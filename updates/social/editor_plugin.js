
(function(tinymce) {
	tinymce.create('tinymce.plugins.SocialPlugin', {
		init : function(ed, url) {
      // Register commands
			ed.addCommand('mceSocial', function() {
				ed.windowManager.open({
					title: 'Example plugin',
          body: [
              {type: 'textbox', name: 'title', label: 'Title'}
          ],
          onsubmit: function(e) {
              // Insert content when the window form is submitted
              ed.insertContent('Title: ' + e.data.title);
          }
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('social', {
        title : 'Twitter', 
        cmd : 'mceSocial'
      });
      
     
    }	
	});

	// Register plugin
	tinymce.PluginManager.add('social', tinymce.plugins.SocialPlugin);
})(tinymce);